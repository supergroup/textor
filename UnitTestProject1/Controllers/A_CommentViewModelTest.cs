﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace Textor.Models
{
    public class A_CommentViewModel
    {
        public string comment { get; set; }
        public string userName { get; set; }
        public DateTime dateCreated { get; set; }
        public string userAvatarUrl { get; set; }
       //int rating { get; set; }
        public int? translationId { get; set; }

        public string mediaName { get; set; }
    }
}
/* hugmynd að view html forritun
 
@model Textor.Models.A_CommentViewModel
@using (Html.BeginForm(null, null, FormMethod.Post, new { enctype = "multipart/form-data" }))
{
    <div>
        @Html.Label(x => x.comment)
        @Html.TextBoxFor(x => x.comment)
    </div>
    <button type="submit">Upload</button>
    
 @using (Html.display)
        @Html.Label(x => x.referenceLink)
        @Html.TextBoxFor(x => x.ReferenceLink)
        @Html.LabelFor(x => x.File)
        @Html.LabelFor(x => x.Language)     
        @Html.TextBoxFor(x => x.File, new { type = "file" })
        @Html.ValidationMessageFor(x => x.File)
    </div>

    <button type="submit">Upload</button>
}


*/