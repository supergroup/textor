﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Textor.Interface;
using Textor.Entity;
using Microsoft.AspNet.Identity.EntityFramework;


namespace Textor.Repositories
{
    // komments
    public class TranslationRepository : ITranslationRepository
    {
        // vantar eitthvad db context dot spyrja halparkokka
        private TextorDataContext dbServer = new TextorDataContext();

        /// <summary>
        /// User dót  done
        /// </summary>
        /// <returns></returns>
        
        public IQueryable<IdentityUser> getUsers() ///DONE
        {
            return dbServer.Users;
        }

        /*
        public ApplicationUser getUserById(string id) ///DONE
        {
            var dbUser = (from u in dbServer.ApplicationUsers
                          where u.Id == id
                          select u).SingleOrDefault();
            return dbUser;
        }
        public void addUser(ApplicationUser user) ///DONE
        {
            dbServer.ApplicationUsers.Add(user);
            save();
        } 
       public void updateUser(ApplicationUser user) ///DONE
        {
            ApplicationUser u = getUserById(user.Id);
            if(u != null)
            {
             //   u.avatarID = user.avatarID;
               // u.email = user.email;
                save();
            }
        }*/
        
        /// <summary>
        /// Avatar dót 
        /// </summary>
        /// <returns></returns>
        public IQueryable<Avatar> getAvatars() ///DONE
        {
            return dbServer.Avatars;
        }
        /*    public Avatar getAvatarById(int id) ///DONE
            {
                var dbAva = (from a in dbServer.Avatars
                             where a.Id == id
                             select a).SingleOrDefault();
                return dbAva;
            }
            public void updateAvatar(int id, string newReference) //Spyrja Hauk
            {
                Avatar avatar = getAvatarById(id);
                if(avatar != null)
                {
                    avatar.imageLocation = newReference;
                    save();
                }
            }
            */
        /// <summary>
        /// Comment dót 
        /// </summary>
        /// <returns></returns>
        public IQueryable<Comment> getComments() ///DONE
        {
            return dbServer.Comments;
        }

        public IQueryable<Comment> getComments(int? userID, int? translationID, int? requestID)
        {
            return dbServer.Comments;
        }

        public void addComment(Comment comment) ///DONE
        {
            dbServer.Comments.Add(comment);
            save();
        }

        /// <summary>
        /// Faq dót 
        /// </summary>
        /// <returns></returns>
        public IQueryable<Faq> getFaqs() ///DONE
        {
            return dbServer.Faqs;
        }

        public void addFaqs(Faq faq) ///DONE
        {
            dbServer.Faqs.Add(faq);
            save();
        }
        /// <summary>
        /// Languages dót
        /// </summary>
        /// <returns></returns>
        public IQueryable<Language> getLanguages() ///DONE
        {
            return dbServer.Languages;
        }
        public Language getLanguageById(int id) ///DONE
        {
            var dbLanguage = (from u in dbServer.Languages
                              where u.Id == id
                              select u).SingleOrDefault();
            return dbLanguage;
        }
        public void addLanguageById(Language lang) ///DONE
        {
            dbServer.Languages.Add(lang);
            save();
        }

        /// <summary>
        /// Media dót
        /// </summary>
        /// <returns></returns>
        public IQueryable<Media> getMedias() ///DONE
        {
            return dbServer.Medias;
        }
        public Media getMediaById(int id) ///DONE
        {
            var dbMed = (from m in dbServer.Medias
                         where m.Id == id
                         select m).SingleOrDefault();
            return dbMed;
        }
        public void addMedia(Media media) ///DONE
        {
            dbServer.Medias.Add(media);
            save();
        }

        /// <summary>
        /// Request dót
        /// </summary>
        /// <returns></returns>
        public IQueryable<Request> getRequests() ///DONE
        {
            return dbServer.Requests;
        }
        public Request getRequestById(int id) ///DONE
        {
            var dbReq = (from r in dbServer.Requests
                         where r.Id == id
                         select r).SingleOrDefault();
            return dbReq;
        }
        public void addRequest(Request request) ///DONE
        {
            dbServer.Requests.Add(request);
            save();
        }
        public void removeRequestById(int id) //Spyrja Hauk
        {
            var dbReq = (from r in dbServer.Requests
                         where r.Id == id
                         select r).SingleOrDefault();
            if (dbReq != null)
            {
                dbServer.Requests.Remove(dbReq);
            }
        }
        public void increaseRequestRating(int id) //spyrja Hauk
        {
            var dbReq = getRequestById(id);
            if (dbReq != null)
            {
                dbReq.rating++;
                save();
            }
        }
        public void decreaseRequestRating(int id) //Spyrja Hauk
        {
            var dbReq = getRequestById(id);
            if (dbReq != null)
            {
                dbReq.rating--;
                save();
            }
        }

        /// <summary>
        /// Subtitles
        /// </summary>
        /// <returns></returns>
        public IQueryable<Subtitle> getSubtitles()
        {
            return dbServer.Subtitles;
        }
        public Subtitle getSubtitleById(int Id) ///DONE
        {
            var dbSub = (from s in dbServer.Subtitles
                         where s.Id == Id
                         select s).SingleOrDefault();
            return dbSub;
        }
        public void addSubtitle(Subtitle subtitle) ///DONE
        {
            dbServer.Subtitles.Add(subtitle);
            save();
        }
        public void increaseSubtitleRating(int id) //Spyrja Hauk
        {
            var dbSub = getSubtitleById(id);
            if (dbSub != null)
            {
                dbSub.rating++;
                save();
            }
        }
        public void decreaseSubtitleRating(int id) //Spyrja Hauk
        {
            var dbSub = getSubtitleById(id);
            if (dbSub != null)
            {
                dbSub.rating--;
                save();
            }
        }

        /// <summary>
        /// Translations
        /// </summary>
        /// <returns></returns>
        public IQueryable<Translation> getTranslations() ///DONE
        {
            return dbServer.Translations;
        }
        public Translation getTranslationById(int Id) ///DONE
        {
            var dbTrans = (from t in dbServer.Translations
                           where t.Id == Id
                           select t).SingleOrDefault();
            return dbTrans;
        }
        public void addTranslation(Translation translation) ///DONE
        {
            dbServer.Translations.Add(translation);
            save();
        }
        public void increaseSTranslationRating(int id) //Spyrja Hauk
        {
            var dbTrans = getTranslationById(id);
            if (dbTrans != null)
            {
                dbTrans.rating++;
                save();
            }
        }
        public void decreaseTranslationRating(int id) //Spyrja Hauk
        {
            var dbTrans = getTranslationById(id);
            if (dbTrans != null)
            {
                dbTrans.rating--;
                save();
            }
        }

        public void save() ///DONE
        {
            dbServer.SaveChanges();
        }


        /*public IQueryable<FileObject> getFileObjects()
        {
            return dbServer.FileObjects;
        }*/ //Í bið
    }
}