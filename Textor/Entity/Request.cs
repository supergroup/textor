﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Textor.Entity
{
    public class Request
    {
        [Key]
        public int Id { get; set; }
        public int mediaID { get; set; }
        public int? rating { get; set; }
    }
}