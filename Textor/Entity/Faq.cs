﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace Textor.Entity
{
    public class Faq
    {
        [Key]
        public int Id { get; set; }
        public string link { get; set; }
        public string question { get; set; }
        public string answer { get; set; }
    }
}