﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Textor.Entity
{
    public class Subtitle
    {
        [Key]
        public int Id { get; set; }
        public string userID { get; set; }
        public int translation { get; set; }
        public int languageID { get; set; }
        public int number { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public int? rating { get; set; }
        public string text { get; set; }
        public DateTime dateCreated { get; set; }
    }
}