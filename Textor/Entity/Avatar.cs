﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Textor.Entity
{
    public class Avatar
    {
        [Key]
        public int Id { get; set; }
        public string imageLocation { get; set; }
    }

}