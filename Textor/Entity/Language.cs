﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Textor.Entity
{
    public class Language
    {
        [Key]
        public int Id { get; set; }
        public string language { get; set; }
    }
}