﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Textor.Entity
{
    public class Translation
    {
        [Key]
        public int Id { get; set; }
        public int mediaID { get; set; }
        public int languageID { get; set; }
        public int? rating { get; set; }
    }
}