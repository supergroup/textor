﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Textor.Entity
{
    public class Media
    {
        [Key]
        public int Id { get; set; }
        public int categoryValue { get; set; }
        public int groupValue { get; set; }
        public string name { get; set; }
        public string referenceLink { get; set; }
    }
}