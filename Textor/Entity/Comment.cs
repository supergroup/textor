﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Textor.Entity
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        public string userID { get; set; }
        public int? translationID { get; set; }
        public int? requestID { get; set; }
        public string text { get; set; }
        public DateTime commentDate { get; set; }

    }
}