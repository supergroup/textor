﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Textor.Entity;
using Textor.Repositories;
using Textor.Models;

namespace Textor.Controllers
{
    public class A_SearchController : ApplicationController
    {
       public A_SearchController()
       {
            repository = new TranslationRepository();
       }
        
       public ActionResult Search()
       {
           return View();
       }

       //Nær í efnið í searchgluggan.
        [HttpPost]
        public ActionResult doSearch(SearchModel search)
        {
            if (!String.IsNullOrEmpty(search.searchField))
            {
                return ShowResults(search.searchField); 
            }
            return View("Search");
        } 

        //Leitar í gegnum media töflunna að efninu
        public ActionResult ShowResults (string str)
        {
            var media = repository.getMedias();

            List<SearchResult> results = new List<SearchResult>();
            if (media != null) 
            {
                foreach (var tmp in media)
                {
                    if (tmp.name.Contains(str) || str.Contains(tmp.name))
                    {
                        SearchResult tmpResult = new SearchResult();
                        tmpResult.mediaName = tmp.name;
						tmpResult.mediaId = tmp.Id;
                        results.Add(tmpResult);
                    }
                }
            return View("ShowResults", results);
            }
            return View("Search");
        }
    }
}
 
