﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Textor.Models;
using Textor.Entity;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Textor.Repositories;

namespace Textor.Controllers
{
    public class A_CommentViewController : ApplicationController
    {
        public ActionResult commentView()
        {
            return View();
        }

		//ToDo add comment by contexts
		[HttpPost]
		public ActionResult addCommentById(int? translationId, int? requestId, string text)
		{
			if(translationId.HasValue)
			{
				Comment tmpComment = new Comment();
				tmpComment.commentDate = DateTime.Now;
				tmpComment.translationID = translationId.Value;
				tmpComment.text = text;
				tmpComment.userID = User.Identity.GetUserId();
				repository.addComment(tmpComment);

				return RedirectToAction("getCommentsById", new { translationId = translationId.Value });
			}
			else if(requestId.HasValue)
			{
				Comment tmpComment = new Comment();
				tmpComment.commentDate = DateTime.Now;
				tmpComment.requestID = requestId.Value;
				tmpComment.text = text;
				tmpComment.userID = User.Identity.GetUserId();
				repository.addComment(tmpComment);

				return RedirectToAction("getCommentsById", new { requestId = requestId.Value });
			}

			return View("Search");
		}

		[HttpGet]
		public ActionResult getCommentsById(int? translationId, int? requestId)
		{
			if(translationId.HasValue)
			{
				List<A_CommentViewModel> commentList = new List<A_CommentViewModel>();
				var comments = (from c in repository.getComments()
							   where c.translationID == translationId.Value
							   select c);
				if(comments != null)
				{
					foreach(var t in comments)
					{
						A_CommentViewModel commentModel = new A_CommentViewModel();
						commentModel.comment = t.text;
						commentModel.dateCreated = t.commentDate;
						commentModel.mediaName = repository.getMediaById(repository.getTranslationById(translationId.Value).mediaID).name;
						commentModel.userName = (from u in repository.getUsers()
												 where u.Id == t.userID
												 select u).SingleOrDefault().UserName;
						commentList.Add(commentModel);
					}
					CommentViewList results = new CommentViewList();
					results.commentModelList = commentList;
					results.translationId = translationId.Value;
					return View("translationComments", results);
				}
			}
			else if(requestId.HasValue)
			{
				List<A_CommentViewModel> commentList = new List<A_CommentViewModel>();
				var comments = (from c in repository.getComments()
							   where c.requestID == requestId.Value
							   select c);
				if (comments != null)
				{
                    
					foreach (var t in comments)
					{
                        var tmpUser = (from u in repository.getUsers()
                                       where u.Id == t.userID
                                       select u).SingleOrDefault();
                        string name;
                        if (tmpUser == null)
                        {
                            name = "Unknown user";
                        }
                        else
                        {
                            name = tmpUser.UserName;
                        }
						A_CommentViewModel commentModel = new A_CommentViewModel();
						commentModel.comment = t.text;
						commentModel.dateCreated = t.commentDate;
						commentModel.mediaName = repository.getMediaById(repository.getRequestById(requestId.Value).mediaID).name;
                        commentModel.userName = name;
						commentList.Add(commentModel);
					}
					CommentViewList results = new CommentViewList();
					results.commentModelList = commentList;
					results.requestId = requestId.Value;
					return View("requestComments", results);
				}
			}
			return View("Search");
		}
		
    }
}