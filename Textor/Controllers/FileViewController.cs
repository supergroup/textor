﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Textor.Models;
using Textor.Entity;

namespace Textor.Controllers
{
    public class FileViewController : ApplicationController
    {
        public FileStreamResult getFile(int translationId, int mediaId)
        {
            FileParserController tmpParser = new FileParserController();

            var tmpFile = tmpParser.buildFile(mediaId, translationId);

            var byteArray = Encoding.ASCII.GetBytes(tmpFile.fileObject);
            var stream = new MemoryStream(byteArray);

            return File(stream, "text/plain", tmpFile.fileName);
        }

		public ActionResult viewFiles(int? mediaId)
		{
			if(mediaId.HasValue)
			{
				var results = (from t in repository.getTranslations()
							   where t.mediaID == mediaId.Value
							   select t);

				if(results != null)
				{
					List<FileViewModel> translationList = new List<FileViewModel>();
					foreach(var t in results)
					{
						FileViewModel tmpTran = new FileViewModel();
						tmpTran.language = repository.getLanguageById(t.languageID).language;
						tmpTran.mediaId = mediaId.Value;
						tmpTran.translationId = t.Id;
						tmpTran.LanguageId = t.languageID;
						tmpTran.mediaName = repository.getMediaById(mediaId.Value).name;
						translationList.Add(tmpTran);
					}

					return View(translationList);
				}

			}
			return View();
		}
    }
}