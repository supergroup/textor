﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Textor.Entity;
using Textor.Models;
using Textor.Repositories;

namespace Textor.Controllers
{
    public class FaqViewController : ApplicationController
    {
        public FaqViewController()
        {
            repository = new TranslationRepository();
        }

        public ActionResult getFaqs() ///Sækir algengar spurningar úr database
        {
            var tmpFaq = repository.getFaqs();

            List<FaqViewModel> faqs = new List<FaqViewModel>();
            foreach (var x in tmpFaq)
            {
                FaqViewModel tmp = new FaqViewModel();
                tmp.question = x.question;
                tmp.answer = x.answer;
                tmp.link = x.link;
                faqs.Add(tmp);
            }

            return View(faqs);
        }
    }
}

