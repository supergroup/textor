﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Textor.Models;
using Textor.Entity;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using System.Text;
using System.IO;



namespace Textor.Controllers
{
    public class A_RequestController : ApplicationController
    {

        public ActionResult requestView()
        {
            return View();
        }

        [HttpPost]
        public ActionResult postRequest(A_RequestModel model)
        {
           
            if(!ModelState.IsValid)
            {
                return RedirectToAction("requestView");
            }

            var tmpMed = (from i in repository.getMedias()
                          where i.name == model.mediaName
                          select i).SingleOrDefault();

            if(tmpMed == null)
            {
                tmpMed = new Media();
                tmpMed.name = model.mediaName;
				tmpMed.groupValue = model.groupValue.Value;
				tmpMed.categoryValue = model.categoryValue.Value;
                tmpMed.referenceLink = model.referenceLink;
                repository.addMedia(tmpMed);

                tmpMed = (from i in repository.getMedias()
                          where i.name == model.mediaName
                          select i).SingleOrDefault();
            }

            if(model.File != null)
            {
                model.userId = User.Identity.GetUserId();

				/*
                string joinedUser = User.Identity.Name;
                var getJoinedUserID = from j in repository.getUsers()
                                      where j.UserName == joinedUser
                                      select j.Id;
                model.userId = getJoinedUserID.First().ToString();
				 */

                //model.userId = "kalli bjarna";

                //Guid userGuid = (Guid)Membership.GetUser().ProviderUserKey;
                //model.userId = userGuid.ToString();

                var langId = (from i in repository.getLanguages()
                              where i.language == model.srtLang
                              select i).SingleOrDefault();
                if (langId == null)
                { 
                    Language newLang = new Language();
                    newLang.language = model.srtLang;
                    repository.addLanguageById(newLang);

                    langId = (from i in repository.getLanguages()
                              where i.language == model.srtLang
                              select i).SingleOrDefault();
                }

                FileParserController parser = new FileParserController();
                if (parser.readFile(model.File, model.userId, langId.Id, tmpMed.Id)) 
                {
                    Request tmpReq = new Request();
                    tmpReq.mediaID = tmpMed.Id;
                    tmpReq.rating = 0;
                    repository.addRequest(tmpReq);

                    return RedirectToAction("requestView");
                }

                else
                {
                    return RedirectToAction("requestView"); //invalid file format
                }

            }

            Request tmpRequ = new Request();
            tmpRequ.mediaID = tmpMed.Id;
            tmpRequ.rating = 0;
            repository.addRequest(tmpRequ);

            return RedirectToAction("requestView");

        }

        [HttpGet]
        public ActionResult getRequests()
        {
            var requests = (from r in repository.getRequests()
                            orderby r.Id descending
                            select r);

            if(requests != null)
            {
                List<A_RequestModel> results = new List<A_RequestModel>();
                foreach(var req in requests)
                {
                    A_RequestModel tmp = new A_RequestModel();
					tmp.requestId = req.Id;
                    tmp.mediaName = repository.getMediaById(req.mediaID).name;
                    tmp.rating = req.rating;
					tmp.mediaId = req.mediaID;             

                    results.Add(tmp);
                }

                return View(results);
            }

            return View();
        }

        public FileStreamResult getFile(int? translationId, int? mediaId)
        {
            FileParserController tmpParser = new FileParserController();

            if (translationId != null && mediaId != null) 
            { 
                var tmpFile = tmpParser.buildFile(Convert.ToInt32(translationId) , Convert.ToInt32(mediaId));

                var byteArray = Encoding.ASCII.GetBytes(tmpFile.fileObject);
                var stream = new MemoryStream(byteArray);
                return File(stream, "text/plain", tmpFile.fileName);
            }
            else
            {
                return null;
            }
        }
        
    }
}/// I am God