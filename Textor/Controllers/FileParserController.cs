﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Textor.Entity;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using System.Text;

namespace Textor.Controllers
{
    public class FileParserController : ApplicationController
    {
        /// <summary>
        /// Fall á að lesa inn srt skrá, skilar bool gildi ef til tekst eður ei
        /// </summary>
        /// <param name="tmpObject"></param>
        /// <param name="applicationUserId"></param>
        /// <param name="langId"></param>
        /// <param name="mediaId"></param>
        /// <returns></returns>
        public bool readFile(HttpPostedFileBase tmpObject, string applicationUserId, int langId, int mediaId)
        {

            var fileExt = System.IO.Path.GetExtension(tmpObject.FileName).Substring(1);

            var supportedExt = new[] { "srt" };

            if (!supportedExt.Contains(fileExt))
            {
                // Ef skráin er ekki .srt skrá
                return false;
            }

            Translation tmpTranslation = new Translation();
            tmpTranslation.languageID = langId;
            tmpTranslation.mediaID = mediaId;
            tmpTranslation.rating = 0;
            repository.addTranslation(tmpTranslation);

            /*var tempTranslation = (from u in repository.getTranslations()
                                            where u.languageID == tmpTranslation.languageID && u.mediaID == tmpTranslation.mediaID
                                            select u).SingleOrDefault();*/
            int translationID = 0;

            if(tmpTranslation != null)
            {
                translationID = tmpTranslation.Id;
            }

            List<string> tmpFile = new List<string>();
            if (tmpObject != null && tmpObject.ContentLength > 0)
            {
                
                byte[] data = new byte[] { };
                using (var binaryReader = new BinaryReader(tmpObject.InputStream))
                {
                    data = binaryReader.ReadBytes(tmpObject.ContentLength);
                }

                char[] data2 = System.Text.Encoding.UTF8.GetString(data).ToCharArray();
                        string line = "";
                        int i = 0;
                        while ((data2.Count()) > i)
                        {
                            if(data[i] != '\n')
                            {
                                if (data[i] != '\r') 
                                {
                                    line += data2[i];
                                }
                            }
                            else
                            {
                                tmpFile.Add(line);
                                line = "";
                            }
                            i++;
                        }
            }
                Subtitle tmpSub = null;
                bool newSub = false;
                for (int i = 0; i < tmpFile.Count; ++i)
                {
                    if (tmpFile[i].Trim().Length == 0)
                    {
                        if (tmpSub == null && newSub == false)
                        {
                            tmpSub = new Subtitle();
                            tmpSub.dateCreated = DateTime.Now;
                            tmpSub.languageID = langId;
                            tmpSub.userID = applicationUserId;
                            tmpSub.translation = translationID;
                            newSub = true;
                        }
                        else if (tmpSub.text != "")
                        {
                            repository.addSubtitle(tmpSub);
                            tmpSub = new Subtitle();
                            tmpSub.dateCreated = DateTime.Now;
                            tmpSub.languageID = langId;
                            tmpSub.translation = translationID;
                            tmpSub.userID = applicationUserId;
                        }
                    }
                    else if (tmpFile[i].Trim().All(char.IsDigit))
                    {
                        if (tmpSub == null)
                        {
                            tmpSub = new Subtitle();
                            tmpSub.dateCreated = DateTime.Now;
                            tmpSub.languageID = langId;
                            tmpSub.userID = applicationUserId;
                            tmpSub.translation = translationID;
                            newSub = true;
                        }
                        tmpSub.number = Int32.Parse(tmpFile[i]);
                    }
                    else if (tmpFile[i].Contains("-->"))
                    {
                        if(tmpSub == null)
                        {
                            tmpSub = new Subtitle();
                            tmpSub.dateCreated = DateTime.Now;
                            tmpSub.languageID = langId;
                            tmpSub.userID = applicationUserId;
                            tmpSub.translation = translationID;
                            newSub = true;
                        }
                        tmpFile[i].Trim();
                        string[] times = new string[]{};
                        times = tmpFile[i].Split(new string[] { "-->" }, StringSplitOptions.RemoveEmptyEntries);
                        tmpSub.startTime = times[0];
                        tmpSub.endTime = times[1];
                  
                    }                    
                    else
                    {
                       /* if (tmpSub.text.Length != 0)
                        {
                            tmpSub.text += tmpFile[i] + "\n";
                        }
                        else
                        { }*/
                        if (tmpSub == null)
                        {
                            tmpSub = new Subtitle();
                            tmpSub.dateCreated = DateTime.Now;
                            tmpSub.languageID = langId;
                            tmpSub.userID = applicationUserId;
                            tmpSub.translation = translationID;
                            newSub = true;
                        }
                            tmpSub.text += tmpFile[i] + "\n";
                        
                    }
                
            }
                if (tmpSub != null) 
                {
                    //if (tmpSub.text.Length != 0) 
                    
                        repository.addSubtitle(tmpSub);
                    
                }
                // ef skráin er lesin inn þá er success
                return true;
            
        }

        /// <summary>
        /// á að skila út srt skrá með nafni myndar og tungumáls
        /// </summary>
        /// <param name="media"></param>
        /// <param name="translationId"></param>
        /// <returns></returns>
        public FileObject buildFile( int translationId, int media)
        {
            var translations = (from u in repository.getSubtitles()
                                where u.translation == translationId
                                orderby u.number ascending,
                                u.dateCreated descending
                                select u);
                                
            int tmp = 0;
            List<Subtitle> subs = new List<Subtitle>();
            foreach (var sub in translations)
            {
                if (sub.number > tmp)
                {
                    tmp = sub.number;
                    subs.Add(sub);
                }
            }

            string medianame = repository.getMediaById(media).name;
            var language = repository.getLanguageById(repository.getTranslationById(translationId).languageID).language;

            if(language != null)
            {
                medianame += language + ".srt";
            }
            else
            {
                medianame += "default.srt";
            }

                StringBuilder writer = new StringBuilder();
                {
                    foreach (var sub in subs)
                    {
                        writer.AppendLine(sub.number.ToString());
                        writer.AppendLine(sub.startTime + "-->" + sub.endTime);
                        writer.AppendLine(sub.text);
                        writer.AppendLine("");
                    }
                }

                FileObject tmpFile = new FileObject();
                tmpFile.fileName = medianame;
                tmpFile.fileObject = writer.ToString();

                return tmpFile;


        }
    }
}