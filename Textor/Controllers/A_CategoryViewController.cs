﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Textor.Models;
using Textor.Entity;

namespace Textor.Controllers
{
    public class A_CategoryViewController : ApplicationController
    {
        public ActionResult categoryView()
        {
            return View();
        }

        [HttpPost]
        public ActionResult getFiles(int mediaId)
        {
            var catMedia = repository.getTranslations();

            List<TranslationModel> results = new List<TranslationModel>();
            if (catMedia != null)
            {
                foreach (var tmp in catMedia)
                {
                    if (tmp.mediaID == mediaId)
                    {
                        TranslationModel tmpResult = new TranslationModel();
                        tmpResult.translationId = tmp.mediaID;
                        tmpResult.languageId = tmp.languageID;
                        //tmpResult.tmpSub = tmp.mediaID;
                        results.Add(tmpResult);
                    }
                }
                return View(results);
            }
            return View();
        }

        /// <summary>
        /// Sækir myndir (text) og raðar eftir flokkum
        /// </summary>
        /// <param name="catValue"></param>
        /// <param name="groValue"></param>
        /// <returns></returns>
        public ActionResult getCategory(int? catValue, int? groValue)
        {
            var category = repository.getMedias();

            if (catValue.HasValue)
            {
                // sækir allar myndir eftir flokkunum kvikmyndir, þættir og barnaefni
                var categoryResult = (from c in repository.getMedias()
                                      where c.categoryValue == catValue.Value
                                      orderby c.name ascending
                                      select c);
                List<CategoryViewModel> mylist = new List<CategoryViewModel>();
                CategoryResult results = new CategoryResult();
                if (groValue.HasValue)
                {
                    // flokkar kvikmyndir, þætti og barnaefni í undirflokka
                    var groupResult = (from s in categoryResult
                                       where s.groupValue == groValue.Value
                                       orderby s.name ascending
                                       select s);


                    foreach (var x in groupResult)
                    {
                        CategoryViewModel show = new CategoryViewModel();
                        show.mediaID = x.Id;
                        show.mediaName = x.name;
                        show.translationId = x.Id;

                        mylist.Add(show);
                    }

                    
                    results.mediaList = mylist;
                    results.categoryValue = catValue.Value;

                    return View(results);
                }

                foreach (var x in categoryResult)
                {
                    CategoryViewModel show = new CategoryViewModel();
                    show.mediaID = x.Id;
                    show.mediaName = x.name;
                    show.translationId = x.Id;

                    mylist.Add(show);

                }

                results.mediaList = mylist;
                results.categoryValue = catValue.Value;

                return View(results);

            }


            return View();
        }


    }
}