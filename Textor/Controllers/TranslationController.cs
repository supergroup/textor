﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Textor.Models;
using Textor.Entity;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using System.Text;
using System.IO;

namespace Textor.Controllers
{
	public class TranslationController : ApplicationController
	{
		public ActionResult translation()
		{
			return View();
		}


		public ActionResult translateMedia(string mediaName, int? translationId, int? langId)
		{

				var mediaId = (from i in repository.getMedias()
							   where i.name == mediaName
							   select i.Id).SingleOrDefault();

				// Ef það kemur inn Id á translation þá byrjum við að undirbúa siebyside translation
				if (translationId.HasValue)
				{

					

					var translation = repository.getTranslationById(translationId.Value);
					List<Subtitle> toBeTranslated = new List<Subtitle>();
					Translation newTranslation;
					if (translation != null)
					{
						if (langId.HasValue)
						{/**/
						
							var checkForTranslation = (from t in repository.getTranslations()
													   where t.languageID == langId.Value && t.mediaID == mediaId
													   select t).FirstOrDefault();

							if(checkForTranslation != null)
							{
								newTranslation = checkForTranslation;
							}
							else
							{
								newTranslation = new Translation();
								newTranslation.languageID = langId.Value;
								newTranslation.mediaID = translation.mediaID;
								newTranslation.rating = 0;
							}
						}
						else
						{
							// frumstillir nýa translation eftir fyrir verandi translation
							// ef ekkert tungumal er valid er sett islensku
							newTranslation = new Translation();
							newTranslation.languageID = 1;
							newTranslation.mediaID = translation.mediaID;
							newTranslation.rating = 0;
						}

						// nær í translation eftir innsendu id raðar eftir number og dagsetningu
						var subsToBeTranslated = (from t in repository.getSubtitles()
													where t.translation == translation.Id
													orderby t.number ascending,
													t.dateCreated descending
													select t);

						// tekur efsta textann í hverju númeri og setur í toBeTranslated listann
						if (subsToBeTranslated != null)
						{
							int tmp = 0;
							foreach (var sub in subsToBeTranslated)
							{
								if (sub.number > tmp)
								{
									tmp = sub.number;
									toBeTranslated.Add(sub);
								}
							}

							//sendir ný Translation niður í gagnagrunninn til að fá Id á það
							if(newTranslation.Id == 0)
							{ 
								repository.addTranslation(newTranslation);
							}
							List<Subtitle> newSubtitles = new List<Subtitle>();

							var checkForSubtitles = (from t in repository.getSubtitles()
													 where t.translation == newTranslation.Id
													 orderby t.number ascending,
													 t.dateCreated descending
													 select t);

							List<Subtitle> test = new List<Subtitle>();
							int tmp2 = 0;
							foreach (var sub in checkForSubtitles)
							{
								if (sub.number > tmp2)
								{
									tmp2 = sub.number;
									test.Add(sub);
								}
							}

							if (test.Count == 0)
							{ 
								// býr til nýan lista af subtitles sem hann sendir síðan til að fá þýðingu á
								foreach(var item in toBeTranslated)
								{
									Subtitle tmpSub = new Subtitle();
									tmpSub.dateCreated = DateTime.Now;
									tmpSub.languageID = newTranslation.languageID;
									tmpSub.number = item.number;
									tmpSub.startTime = item.startTime;
									tmpSub.endTime = item.endTime;
									tmpSub.translation = newTranslation.Id;
									tmpSub.userID = User.Identity.GetUserId();
									newSubtitles.Add(tmpSub);
								}
							}
							else
							{
								newSubtitles = test;
							}

							// Býr til translation View model fyrir sidebyside translation
							TranslationModel model = new TranslationModel();
							model.toTranslate = toBeTranslated;
							model.translated = newSubtitles;
							model.languageId = newTranslation.languageID;
							model.translationId = newTranslation.Id;
							return sideBySide(model);
						}
					}
				}
			
			// sendir í tóma translation ef það var ekki til skrá til að þýða
			return View("translation");
		}

		[HttpPost]
		[ValidateInput(false)]
		public ActionResult saveSubtitle(string text, 
											int translationId,
											int number,
											string startTime,
											string endTime)
		{
			// Checka hvort það sé til færsla í translation töflu m.v. mediaid og languageid
			var translation = repository.getTranslationById(translationId);
			if(translation != null)
			{
				Subtitle newSub = new Subtitle();
				newSub.translation = translation.Id;
				newSub.languageID = translation.languageID;
				newSub.dateCreated = DateTime.Now;
				newSub.rating = 0;
				newSub.number = number;
				newSub.userID = User.Identity.GetUserId();
				newSub.text = text;
				newSub.startTime = startTime;
				newSub.endTime = endTime;
				repository.addSubtitle(newSub);
			}

			return addSubtitle(translationId);
		}

		[HttpPost]
		[ValidateInput(false)] //gert ovirkt
		public ActionResult newTranslation(string mediaName, int? languageId, int? categoryValue, int? groupValue)
		{
			if(!ModelState.IsValid)
			{
				return View("translation");
			}
				int mediaId = (from i in repository.getMedias()
							   where i.name == mediaName
							   select i.Id).SingleOrDefault();

				if(mediaId == 0)
				{
					Media tmpMedia = new Media();
					tmpMedia.name = mediaName;
					tmpMedia.categoryValue = categoryValue.Value;
					tmpMedia.groupValue = groupValue.Value;
					repository.addMedia(tmpMedia);

					mediaId = (from i in repository.getMedias()
							   where i.name == mediaName
							   select i.Id).SingleOrDefault();
				}

				
				
				// Býr til nýtt translation og frumstillir
				Translation newTranslation = new Translation();
				if(languageId.HasValue)
				{
					newTranslation.languageID = languageId.Value;
				}
				else
				{
					newTranslation.languageID = 1;
				}
				newTranslation.mediaID = mediaId;
				newTranslation.rating = 0;
				repository.addTranslation(newTranslation);

				var repoTranslation = (from s in repository.getTranslations()
								  where s.mediaID == mediaId && s.languageID == languageId.Value
								  select s).FirstOrDefault();


				return addSubtitle(repoTranslation.Id);
			
		}

		[HttpGet]
		public ActionResult addSubtitle(int translationId)
		{
			var tmpTranslation = repository.getTranslationById(translationId);

			if(tmpTranslation != null)
			{ 
				TranslationModel model = new TranslationModel();
				model.translated = new List<Subtitle>();
			
				// nær í translation eftir innsendu id raðar eftir number og dagsetningu
				var subsToBeTranslated = (from t in repository.getSubtitles()
											where t.translation == translationId
											orderby t.number ascending,
											t.dateCreated descending
											select t);

				// tekur nyasta textann í hverju númeri og setur í toBeTranslated listann
				if (subsToBeTranslated != null)
				{
					int tmp = 0;
					foreach (var sub in subsToBeTranslated)
					{
						if (sub.number > tmp)
						{
							tmp = sub.number;
							model.translated.Add(sub);
						}
					}
				}
				model.languageId = tmpTranslation.languageID;
				model.translationId = tmpTranslation.Id;

				return View("singleTranslate", model);
			}

			return View("Error");
		}

		public ActionResult sideBySide(TranslationModel model)
		{
			return View("sideBySide", model);
		}

		public ActionResult singleTranslate(TranslationModel model)
		{
			return View("singleTranslate", model);
		}

		[HttpPost]
		[ValidateInput(false)]
		public ActionResult commitTranslation(TranslationModel model)
		{
			if (!ModelState.IsValid)
			{
				return View("Error");
			}

			foreach(var t in model.translated)
			{
					t.userID = User.Identity.GetUserId();
					t.dateCreated = DateTime.Now;
					repository.addSubtitle(t);
			}

			return View("Search");
		}
	}
}