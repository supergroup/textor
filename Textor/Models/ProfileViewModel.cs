﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Textor.Models
{
    public class ProfileViewModel
    {
        public string name { get; set; }
        public string email { get; set; }
        public List<int> myComments { get; set; }
        public List<int> myTranslations { get; set; }
        public List<int> mySubtitles { get; set; }
        public string avatarLocation { get; set; }
    }
}