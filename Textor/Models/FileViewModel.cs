﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Textor.Models
{
    public class FileViewModel
    {
		public int translationId { get; set; }
		public int mediaId { get; set; }
		public int LanguageId { get; set; }
		public string language { get; set; }
		public string mediaName { get; set; }
    }
}