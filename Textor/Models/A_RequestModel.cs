﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Textor.Models
{
    public class A_RequestModel
    {
        [Required]
        public string mediaName { get; set; }
		public int? categoryValue { get; set; }
		public int? groupValue { get; set; }
		public int? requestId { get; set; }
        public string referenceLink { get; set; }
        public int? rating { get; set; }
        public string userId { get; set; }
        public HttpPostedFileBase File { get; set; }
        public string srtLang { get; set; }
        public int? mediaId { get; set; }
    }
}

/* hugmynd að view html forritun
 
@model Textor.Models.A_RequestModel
@using (Html.BeginForm(null, null, FormMethod.Post, new { enctype = "multipart/form-data" }))
{
    <div>
        @Html.Label(x => x.mediaName)
        @Html.TextBoxFor(x => x.mediaName)
        @Html.Label(x => x.referenceLink)
        @Html.TextBoxFor(x => x.ReferenceLink)
        @Html.LabelFor(x => x.File)
        @Html.LabelFor(x => x.Language)     
        @Html.TextBoxFor(x => x.File, new { type = "file" })
        @Html.ValidationMessageFor(x => x.File)
    </div>

    <button type="submit">Upload</button>
}


*/