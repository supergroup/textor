﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Textor.Models
{
    public class FaqViewModel
    {
        public string question { get; set; }
        public string link { get; set; }
        public string answer { get; set; }
    }
}