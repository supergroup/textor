﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Textor.Entity;


namespace Textor.Models
{
    public class TranslationModel
    {
        public List<Subtitle> toTranslate { get; set; }
        public List<Subtitle> translated { get; set; }
        public int languageId { get; set; }
		public int translationId { get; set; }
		public Subtitle tmpSub { get; set; }
    }
    public class LinkToTranslationModel
    {
        public int mediaId { get; set; }
    }
}