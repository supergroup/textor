﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Textor.Models
{
	public class CommentViewList
	{
		public int? requestId { get; set; }
		public int? translationId { get; set; }
		public List<A_CommentViewModel> commentModelList { get; set; }
	}
}