﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Textor.Models
{
    public class CategoryViewModel
    {
        public int mediaID { get; set; }
        public string mediaName { get; set; }
        public int translationId { get; set; }
    }

    public class CategoryResult
    {
        public List<CategoryViewModel> mediaList { get; set; }
        public int categoryValue { get; set; }
    }


}