﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Textor.Models
{
    public class SearchModel
    {
        public string searchField { get; set; }
    }

    public class SearchResult
    {
        public string mediaName { get; set; }
		public int mediaId { get; set; }
    }

}