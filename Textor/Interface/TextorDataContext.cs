﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web;
using Textor.Entity;
using System.ComponentModel.DataAnnotations;

namespace Textor.Interface
{
    public class TextorDataContext : ApplicationDbContext
    {

        //public DbSet<ApplicationUser> ApplicationUsers { get; set; }
		public DbSet<IdentityUser> Users { get; set; }
        
        public DbSet<Avatar> Avatars { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<Faq> Faqs { get; set; }

        //public DbSet<FileObject> FileObjects { get; set; }

        public DbSet<Language> Languages { get; set; }

        public DbSet<Media> Medias { get; set; }

        public DbSet<Request> Requests { get; set; }

        public DbSet<Subtitle> Subtitles { get; set; }

        public DbSet<Translation> Translations { get; set; }
        
    }
}