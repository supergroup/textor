﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Textor.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Textor.Interface
{
    public interface ITranslationRepository
    {
        /// <summary>
        /// User dót  done
        /// </summary>
        /// <returns></returns>
        
        IQueryable<IdentityUser> getUsers();
        /*
        ApplicationUser getUserById(string id);
        void addUser(ApplicationUser user);
        void updateUser(ApplicationUser user);*/

        /// <summary>
        /// Avatar dót 
        /// </summary>
        /// <returns></returns>
        IQueryable<Avatar> getAvatars();
      //  Avatar getAvatarById(int id);
      //  void updateAvatar(int id, string newReference);

        /// <summary>
        /// Comment dót 
        /// </summary>
        /// <returns></returns>
        IQueryable<Comment> getComments();
        IQueryable<Comment> getComments(int? userID, int? translationID, int? requestID);
        void addComment(Comment comment);

        /// <summary>
        /// Faq dót 
        /// </summary>
        /// <returns></returns>
        IQueryable<Faq> getFaqs();
        void addFaqs(Faq faq);

        /// <summary>
        /// Languages dót
        /// </summary>
        /// <returns></returns>
        IQueryable<Language> getLanguages();
        Language getLanguageById(int id);
        void addLanguageById(Language lang);

        /// <summary>
        /// Media dót
        /// </summary>
        /// <returns></returns>
        IQueryable<Media> getMedias();
        Media getMediaById(int id);
        void addMedia(Media media);


        /// <summary>
        /// Request dót
        /// </summary>
        /// <returns></returns>
        IQueryable<Request> getRequests();
        Request getRequestById(int id);
        void addRequest(Request request);
        void removeRequestById(int id);
        void increaseRequestRating(int id);
        void decreaseRequestRating(int id);

        /// <summary>
        /// Subtitles
        /// </summary>
        /// <returns></returns>
        IQueryable<Subtitle> getSubtitles();
        Subtitle getSubtitleById(int Id);
        void addSubtitle(Subtitle subtitle);
        void increaseSubtitleRating(int id);
        void decreaseSubtitleRating(int id);

        /// <summary>
        /// Translations
        /// </summary>
        /// <returns></returns>
        IQueryable<Translation> getTranslations();
        Translation getTranslationById(int Id);
        void addTranslation(Translation translation);
        void increaseSTranslationRating(int id);
        void decreaseTranslationRating(int id);

        void save();

        //IQueryable<FileObject> getFileObjects(); Í bið

        
    }
}
