﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Textor.Startup))]
namespace Textor
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
